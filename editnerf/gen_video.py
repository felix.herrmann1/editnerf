import torch
import os
import numpy as np
import torchvision
from tqdm import tqdm
import glob
import imageio
import json

from rendering import render
from inputs import config_parser
from model import create_nerf
from load_blender import pose_spherical
from run_nerf_helpers import to8b, get_rays

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
np.random.seed(0)

def load_data(basedir, args):
    with open(os.path.join(basedir, 'instances.txt')) as f:
            instances = [x.strip() for x in f.readlines()]
            instance_names = [os.path.join(basedir, instance_name) for instance_name in instances]
            if args.instance >= 0:
                instance_names = [instance_names[args.instance]]
    inst_dir = os.path.join(basedir, instance_names[0])
    with open(os.path.join(inst_dir, 'transforms_test.json')) as f:
        meta = json.load(f)
        H, W = imageio.imread(os.path.join(inst_dir, meta['frames'][0]['file_path'] + '.png')).shape[:2]
        radius = np.linalg.norm(np.array(meta['frames'][0]['transform_matrix'])[:3, -1])
        if 'focal' not in meta:
            camera_angle_x = float(meta['camera_angle_x'])
            focal = .5 * W / np.tan(.5 * camera_angle_x)
        else:
            focal = meta['focal']
    return H, W, focal, radius, instance_names

def render_circle():
    parser = config_parser()
    parser.add_argument(
        "--focal", type=float, default=-1, help="focal length"
    )
    parser.add_argument(
        "--radius", type=float, default=-1, help="render radius"
    )
    parser.add_argument(
        "--elevation", type=float, default=-45.0, help="Elevation angle (negative is above)"
    )
    parser.add_argument(
        "--trans", type=float, default=0, help="z-translation"
    )
    parser.add_argument(
        "--n_frames", type=int, default=50, help="number of rendered frames"
    )
    args = parser.parse_args()

    # Multi-GPU
    args.n_gpus = torch.cuda.device_count()
    print(f"Using {args.n_gpus} GPU(s).")

    radius = args.radius
    elevation = args.elevation
    instance = args.instance


    H, W, focal, radius, instances = load_data(args.datadir, args)

    if args.radius != -1:
        radius = args.radius
    if args.size != -1:
        H, W = args.size
    if args.focal != -1:
        focal = args.focal
    if args.n_frames == -1:
        args.n_frames = 50
    if args.instance == -1:
        instance = 0

    focal = torch.tensor(focal)

    images = []
    disps = []

    near = args.blender_near
    far = args.blender_far
    bds_dict = {'near': near, 'far': far}

    render_poses = torch.stack([pose_spherical(angle, elevation, radius) for angle in np.linspace(-180, 180, args.n_frames + 1)[:-1]])

    render_poses[:,2, -1] += args.trans

    basedir = args.basedir
    expname = args.expname

    print("Rendering video for {}".format(expname))
    print("Image size: {}x{}".format(H, W))
    print("Focal: {}".format(focal))
    print("Near: {}".format(near))
    print("Far: {}".format(far))
    print("Radius: {}".format(radius))
    print("Number of frames: {}".format(args.n_frames))



    _, render_kwargs_test, _, _, _, styles = create_nerf(args, return_styles=True)
    render_kwargs_test.update(bds_dict)
    with torch.no_grad():
        testsavedir = os.path.join(basedir, expname, "video")
        if not os.path.exists(testsavedir):
            os.makedirs(testsavedir)
        else:
            files = glob.glob(os.path.join(testsavedir, "*.png"))
            for f in files:
                os.remove(f)
        for i, pose in tqdm(enumerate(render_poses)):
            rays = get_rays(H, W, focal.to(device), c2w=pose[:3,:4].to(device))
            style = torch.stack([styles[instance] for _ in range(H*W)]).detach()
            rgb, disp, _, _ = render(H, W, focal.to(device), style=style.to(device), chunk=args.chunk*args.n_gpus,
                   rays=(rays[0].to(device), rays[1].to(device)),
                   **render_kwargs_test)

            rgb_path = os.path.join(testsavedir, 'rgb_{:02d}.png'.format(i))
            disp_path = os.path.join(testsavedir, 'disp_{:02d}.png'.format(i))

            torchvision.utils.save_image(rgb.permute(2, 0, 1), rgb_path)
            torchvision.utils.save_image(disp.unsqueeze(0), disp_path)

            images.append(rgb.cpu().numpy())
            disps.append(disp.cpu().numpy())
    
    imageio.mimsave(os.path.join(testsavedir,'rgb.gif'), images)
    imageio.mimsave(os.path.join(testsavedir,'disp.gif'), disps)

if __name__ == '__main__':
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    render_circle()
