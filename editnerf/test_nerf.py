import random

import torch
import os
import numpy as np

from rendering import render_path
from dataset import load_data
from inputs import config_parser
from model import create_nerf
from load_blender import pose_spherical
from run_nerf_helpers import to8b
import imageio


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
np.random.seed(0)


def test():
    parser = config_parser()
    parser.add_argument(
    "--n_frames", type=int, default=90, help="number of rendered frames"
    )
    args = parser.parse_args()

    images, poses, style, i_test, i_train, _, bds_dict, dataset, hwfs, near_fars, _, _ = load_data(args)
    images_test, poses_test, style_test, hwfs_test, nf_test = images[i_test], poses[i_test], style[i_test], hwfs[i_test], near_fars[i_test]
    images_train, poses_train, style_train, hwfs_train, nf_train = images[i_train], poses[i_train], style[i_train], hwfs[i_train], near_fars[i_train]

    # Create log dir and copy the config file
    basedir = args.basedir
    expname = args.expname
    render_kwargs_train, render_kwargs_test, start, grad_vars, optimizer, _ = create_nerf(args)

    # Multi-GPU
    args.n_gpus = torch.cuda.device_count()
    print(f"Using {args.n_gpus} GPU(s).")

    np.save(os.path.join(basedir, expname, 'poses.npy'), poses_train.cpu())
    np.save(os.path.join(basedir, expname, 'hwfs.npy'), hwfs_train.cpu())

    render_kwargs_train.update(bds_dict)
    render_kwargs_test.update(bds_dict)

    with torch.no_grad():
        if args.render_test:
            if args.shuffle_poses:
                print('Shuffling test poses')
                permutation = list(range(len(poses_test)))
                random.shuffle(permutation)
                poses_test = poses_test[permutation]
            testsavedir = os.path.join(basedir, expname, 'test_imgs{:06d}'.format(start))
            os.makedirs(testsavedir, exist_ok=True)
            _, _, psnr = render_path(poses_test.to(device), style_test, hwfs_test, args.chunk, render_kwargs_test, nfs=nf_test, gt_imgs=images_test, savedir=testsavedir)
            print('Saved test set w/ psnr', psnr)

        if args.render_train:
            if args.shuffle_poses:
                print('Shuffling train poses')
                permutation = list(range(len(poses_train)))
                random.shuffle(permutation)
                poses_train = poses_train[permutation]
            trainsavedir = os.path.join(basedir, expname, 'train_imgs{:06d}'.format(start))
            os.makedirs(trainsavedir, exist_ok=True)
            _, _, psnr = render_path(poses_train.to(device), style_train, hwfs_train, args.chunk, render_kwargs_test, nfs=nf_train, gt_imgs=images_train, savedir=trainsavedir)
            print('Saved train set w/ psnr', psnr)
        if args.render_gif:
            i_instance = args.instance if args.instance != -1 else 0
            gifsavedir = os.path.join(basedir, expname, '{}_circle_{:06d}_'.format(expname, start))
            os.makedirs(gifsavedir, exist_ok=True)
            gif_styles = dataset.style[i_instance].repeat(args.n_frames,1)
            gif_hwfs = hwfs[0].repeat(args.n_frames,1)
            gif_nfs = near_fars[0].repeat(args.n_frames,1)
            poses = torch.stack([pose_spherical(angle, -35, 1.581139) for angle in np.linspace(-180, 180, args.n_frames + 1)[:-1]])
            rgbs, disps, psnr = render_path(poses.to(device), gif_styles, gif_hwfs, args.chunk, render_kwargs_test, nfs=gif_nfs, gt_imgs=None, savedir=gifsavedir)
            imageio.mimwrite(gifsavedir + 'rgb.mp4', to8b(rgbs), fps=30, quality=8)
            imageio.mimwrite(gifsavedir + 'disp.mp4', to8b(disps / np.max(disps)), fps=30, quality=8)
            imageio.mimsave(gifsavedir + 'rgb.gif', to8b(rgbs), fps=20)
            imageio.mimsave(gifsavedir + 'disp.gif', to8b(disps), fps=20)
            print('Saved train set w/ psnr', psnr)




if __name__ == '__main__':
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    test()
