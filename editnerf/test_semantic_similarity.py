import os
import copy
import sys

import numpy as np
import torch
from torch.utils.checkpoint import checkpoint as run_checkpoint
import torch.nn.functional as F
from scipy.spatial.transform import Rotation

import imageio
import wandb
import glob

from run_nerf_helpers import img2mse, mse2psnr, to8b, make_wandb_image, get_embed_fn, get_rays
import geometry
from inputs import config_parser
from dataset import load_data
from model import create_nerf
from rendering import render, render_path
from utils.pidfile import exit_if_job_done, mark_job_done
from load_blender import pose_spherical_uniform, pose_spherical

import matplotlib.pyplot as plt

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def train():
    parser = config_parser()
    args = parser.parse_args()
    args.render_test = True

    # Load data
    images, _, _, _, _, _, _, dataset, _, _, _, _ = load_data(args)

    if args.consistency_loss != 'none':
        print(f'Using auxilliary consistency loss [{args.consistency_loss}], fine weight [{args.consistency_loss_lam}], coarse weight [{args.consistency_loss_lam0}]')
        embed = get_embed_fn(args.consistency_model_type, args.consistency_model_num_layers, checkpoint=args.checkpoint_embedding, clip_cache_root=args.clip_cache_root)

    calc_ctr_loss = args.consistency_loss.startswith('consistent_with_target_rep')
    any_rendered_loss = calc_ctr_loss
    if any_rendered_loss:
        print("interpolating target images using mode: {}".format(args.pixel_interp_mode))
        with torch.no_grad():
            targets = images.permute(0, 3, 1, 2).to(device)
            if args.render_nH < args.consistency_size or args.render_nW < args.consistency_size:
                targets = F.interpolate(targets, (args.render_nH, args.render_nW),
                                                     mode=args.pixel_interp_mode)

        # Embed training images for consistency loss
        if calc_ctr_loss:
            with torch.no_grad():
                targets_resize_model = F.interpolate(targets, (args.consistency_size, args.consistency_size),
                                                     mode=args.pixel_interp_mode)
                target_embeddings = embed(targets_resize_model)  # [N,L,D]

    # test within instance consistency
    target_cossim = []
    for i_target_emb in range(args.N_instances):
        target_embeds = target_embeddings[dataset.instance_to_train_image_id[i_target_emb], 0] 
        target_cossim += [torch.cosine_similarity(target_embeds[0], target_embeds[i], dim=-1).item() for i in range(target_embeds.shape[0])]

    target_cossim = np.stack(target_cossim)
    hist, bins = np.histogram(target_cossim, 40, density=True)
    cossim_density = hist / np.sum(hist)
    widths = bins[:-1] - bins[1:]
    fig, ax = plt.subplots()
    ax.bar(bins[1:], cossim_density, width=widths)


    ax.set_xlabel('Cosine Sim')
    ax.set_ylabel('Probability')
    ax.set_title('Within object cosine similarity, instances: {}, resolution: ({}x{})'.format(args.N_instances, args.render_nH, args.render_nW))
    ax.set_xlim(0, 1)
    ax.grid(True)

    plotdir = './plots/within_cosine_similarity_{}x{}.png'.format(args.render_nH, args.render_nW)
    fig.savefig(plotdir)
    print("wrote plot to: {}".format(plotdir))

    # test between instance consistency
    target_cossim = []
    for i_target_emb in range(args.N_instances):
        for i_target_emb_other in range(i_target_emb+1, args.N_instances):
            target_embed = target_embeddings[np.random.choice(dataset.instance_to_train_image_id[i_target_emb]), 0]
            target_embeds_other = target_embeddings[dataset.instance_to_train_image_id[i_target_emb_other], 0] 
            target_cossim += [torch.cosine_similarity(target_embed, target_embeds_other[i], dim=-1).item() for i in range(target_embeds_other.shape[0])]

    target_cossim = np.stack(target_cossim)
    hist, bins = np.histogram(target_cossim, 40, density=True)
    cossim_density = hist / np.sum(hist)
    widths = bins[:-1] - bins[1:]
    fig, ax = plt.subplots()
    ax.bar(bins[1:], cossim_density, width=widths)

    ax.set_xlabel('Cosine Sim')
    ax.set_ylabel('Probability')
    ax.set_title('Between object cosine similarity, instances: {}, resolution: ({}x{})'.format(args.N_instances, args.render_nH, args.render_nW))
    ax.set_xlim(0, 1)
    ax.grid(True)

    plotdir = './plots/between_cosine_similarity_{}x{}.png'.format(args.render_nH, args.render_nW)
    fig.savefig(plotdir)
    print("wrote plot to: {}".format(plotdir))

if __name__ == '__main__':
    parser = config_parser()
    args = parser.parse_args()
    if args.instance != -1:
        # Allows for scripting over single instance experiments.
        exit_if_job_done(os.path.join(args.basedir, args.expname))
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        train()
        mark_job_done(os.path.join(args.basedir, args.expname))
    else:
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        train()
