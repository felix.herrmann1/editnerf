from torchsearchsorted import searchsorted
import numpy as np
import torch
import wandb
import torch.nn as nn
import torchvision
import timm
import clip_utils

torch.autograd.set_detect_anomaly(True)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

TEST = False

# Misc


def img2mse(x, y): return torch.mean((x - y) ** 2)


def img2l1(x, y): return torch.mean((x - y).abs())
def mse2psnr(x): return -10. * torch.log(x) / torch.log(torch.Tensor([10.]))


def to8b(x): return (255 * np.clip(x, 0, 1)).astype(np.uint8)

def get_embed_fn(model_type, num_layers=-1, spatial=False, checkpoint=False, clip_cache_root=None):
    if model_type.startswith('clip_'):
        if model_type == 'clip_rn50':
            assert clip_cache_root
            clip_utils.load_rn(jit=False, root=clip_cache_root)
            if spatial:
                _clip_dtype = clip_utils.clip_model_rn.clip_model.dtype
                assert num_layers == -1
                def embed(ims):
                    ims = clip_utils.CLIP_NORMALIZE(ims).type(_clip_dtype)
                    return clip_utils.clip_model_rn.clip_model.visual.featurize(ims)  # [N,C,56,56]
            else:
                embed = lambda ims: clip_utils.clip_model_rn(images_or_text=clip_utils.CLIP_NORMALIZE(ims), num_layers=num_layers).unsqueeze(1)
            assert not clip_utils.clip_model_rn.training
        elif model_type.startswith('clip_vit'):
            assert clip_cache_root
            if model_type == 'clip_vit':
                clip_utils.load_vit(root=clip_cache_root)
            elif model_type == 'clip_vit_b16':
                clip_utils.load_vit('ViT-B/16', root=clip_cache_root)
            if spatial:
                def embed(ims):
                    emb = clip_utils.clip_model_vit(images_or_text=clip_utils.CLIP_NORMALIZE(ims), num_layers=num_layers)  # [N,L=50,D]
                    return emb[:, 1:].view(emb.shape[0], 7, 7, emb.shape[2]).permute(0, 3, 1, 2)  # [N,D,7,7]
            else:
                embed = lambda ims: clip_utils.clip_model_vit(images_or_text=clip_utils.CLIP_NORMALIZE(ims), num_layers=num_layers)  # [N,L=50,D]
            assert not clip_utils.clip_model_vit.training
        elif model_type == 'clip_rn50x4':
            assert not spatial
            clip_utils.load_rn(name='RN50x4', jit=False)
            assert not clip_utils.clip_model_rn.training
            embed = lambda ims: clip_utils.clip_model_rn(images_or_text=clip_utils.CLIP_NORMALIZE(ims), featurize=False)
    elif model_type.startswith('timm_'):
        assert num_layers == -1
        assert not spatial

        model_type = model_type[len('timm_'):]
        encoder = timm.create_model(model_type, pretrained=True, num_classes=0)
        encoder.eval()
        normalize = torchvision.transforms.Normalize(
            encoder.default_cfg['mean'], encoder.default_cfg['std'])  # normalize an image that is already scaled to [0, 1]
        encoder = nn.DataParallel(encoder).to(device)
        embed = lambda ims: encoder(normalize(ims)).unsqueeze(1)
    elif model_type.startswith('torch_'):
        assert num_layers == -1
        assert not spatial

        model_type = model_type[len('torch_'):]
        encoder = torch.hub.load('pytorch/vision:v0.6.0', model_type, pretrained=True)
        encoder.eval()
        encoder = nn.DataParallel(encoder).to(device)
        normalize = torchvision.transforms.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])  # normalize an image that is already scaled to [0, 1]
        embed = lambda ims: encoder(normalize(ims)).unsqueeze(1)
    else:
        raise ValueError

    if checkpoint:
        return lambda x: run_checkpoint(embed, x)

    return embed


@torch.no_grad()
def make_wandb_image(tensor, preprocess='scale'):
    tensor = tensor.detach()
    tensor = tensor.float()
    if preprocess == 'scale':
        mi = tensor.min()
        tensor = ((tensor - mi) / (tensor.max() - mi))
    elif preprocess == 'clip':
        tensor = tensor.clip(0, 1)
    return wandb.Image(tensor.cpu().numpy())

def gradient_norm(parameters):
    # https://discuss.pytorch.org/t/check-the-norm-of-gradients/27961
    total_norm = 0.
    for p in parameters:
        if p.grad is not None:
            param_norm = p.grad.data.norm(2)
            total_norm += param_norm.item() ** 2
    total_norm = total_norm ** (1. / 2)
    return total_norm

def to_disp_img(disp):
    # clip outliers
    #disp = 1. / disp
    min_disp, max_disp = np.percentile(disp, [5, 95])
    disp[disp < min_disp] = min_disp
    disp[disp > max_disp] = max_disp

    # disp = disp - disp.min() #normalize to have [0, max]
    disp = disp / disp.max()  # normalize in [0, 1]

    return disp

# Ray helpers


def get_rays(H, W, focal, c2w, nH=None, nW=None):
    if nH is None:
        nH = H
    if nW is None:
        nW = W
    #TODO: implement jitter from DietNeRF
    pts_W = torch.linspace(0, W - 1, nW)
    pts_H = torch.linspace(0, H - 1, nH)

    i, j = torch.meshgrid(pts_W, pts_H, indexing='ij')  # pytorch's meshgrid has indexing='ij'
    i = i.t()
    j = j.t()
    wfactor, hfactor = focal.item(), focal.item()
    if focal < 10:  # super hacky
        # normalize to [-1, 1]
        wfactor *= (W * .5)
        hfactor *= (H * .5)
        # inside [-200, 200] (400/2), we only want to render from [-128/200, 128/200]
        wfactor *= (200. / 128.)
        hfactor *= (200. / 128.)
    dirs = torch.stack([(i - W * .5) / wfactor, -(j - H * .5) / hfactor, -torch.ones_like(i)], -1)
    # Rotate ray directions from camera frame to the world frame
    rays_d = torch.sum(dirs[..., np.newaxis, :] * c2w[:3, :3], -1)  # dot product, equals to: [c2w.dot(dir) for dir in dirs]
    # Translate camera frame's origin to the world frame. It is the origin of all rays.
    rays_o = c2w[:3, -1].expand(rays_d.shape)
    return rays_o, rays_d

# Hierarchical sampling (section 5.2)


def sample_pdf(bins, weights, N_samples, det=False, pytest=False):
    # Get pdf
    weights = weights + 1e-5  # prevent nans
    pdf = weights / torch.sum(weights, -1, keepdim=True)
    cdf = torch.cumsum(pdf, -1)
    cdf = torch.cat([torch.zeros_like(cdf[..., :1]), cdf], -1)  # (batch, len(bins))

    # Take uniform samples
    if det:
        u = torch.linspace(0., 1., steps=N_samples)
        u = u.expand(list(cdf.shape[:-1]) + [N_samples])
    else:
        u = torch.rand(list(cdf.shape[:-1]) + [N_samples])

    # Pytest, overwrite u with numpy's fixed random numbers
    if pytest:
        np.random.seed(0)
        new_shape = list(cdf.shape[:-1]) + [N_samples]
        if det:
            u = np.linspace(0., 1., N_samples)
            u = np.broadcast_to(u, new_shape)
        else:
            u = np.random.rand(*new_shape)
        u = torch.Tensor(u)

    # Invert CDF
    u = u.contiguous()
    inds = searchsorted(cdf, u, side='right')
    below = torch.max(torch.zeros_like(inds - 1), inds - 1)
    above = torch.min((cdf.shape[-1] - 1) * torch.ones_like(inds), inds)
    inds_g = torch.stack([below, above], -1)  # (batch, N_samples, 2)

    matched_shape = [inds_g.shape[0], inds_g.shape[1], cdf.shape[-1]]
    cdf_g = torch.gather(cdf.unsqueeze(1).expand(matched_shape), 2, inds_g)
    bins_g = torch.gather(bins.unsqueeze(1).expand(matched_shape), 2, inds_g)

    denom = (cdf_g[..., 1] - cdf_g[..., 0])
    denom = torch.where(denom < 1e-5, torch.ones_like(denom), denom)
    t = (u - cdf_g[..., 0]) / denom
    samples = bins_g[..., 0] + t * (bins_g[..., 1] - bins_g[..., 0])

    return samples
