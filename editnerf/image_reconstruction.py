from email.policy import default
import os
import copy
import sys

import numpy as np
import torch
from torch.utils.checkpoint import checkpoint as run_checkpoint
import torch.nn.functional as F
from scipy.spatial.transform import Rotation

import imageio
import wandb
import glob

from run_nerf_helpers import img2mse, mse2psnr, to8b, make_wandb_image, get_embed_fn, get_rays
import geometry
from inputs import config_parser
from dataset import load_data
from model import create_nerf
from rendering import render, render_path
from utils.pidfile import exit_if_job_done, mark_job_done
from load_blender import pose_spherical_uniform, pose_spherical


import configargparse
import torch.nn.functional as F
import torch.nn as nn
import torch
from torch.nn.modules.activation import ReLU
from models.embedders import get_embedder


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class MLP(nn.Module):
    def __init__(self, depth=8, dim=128, input_ch=2, output_ch=3, activation="softplus"):
        super().__init__()
        self.activation = F.relu

        first_block = nn.Linear(input_ch, dim)
        self.last_block = nn.Linear(dim, output_ch)
        self.mlp = nn.ModuleList([first_block] + [nn.Linear(dim, dim) for _ in range(depth - 1)])

    def forward(self, x):
        for i, layer in enumerate(self.mlp):
            x = self.activation(layer(x))
        x = torch.sigmoid(self.last_block(x))
        return x

def run_MLP(inputs, fn, embed_fn):
    inputs_flat = torch.reshape(inputs, [-1, inputs.shape[-1]])
    embedded = embed_fn(inputs_flat)
    outputs_flat = fn(embedded)
    outputs = torch.reshape(outputs_flat, list(inputs.shape[:-1]) + [outputs_flat.shape[-1]])
    return outputs

def create_MLP(args):
    embed_fn, input_ch = get_embedder(args.multires, args.i_embed)
    model = MLP(depth=args.D_mlp, dim=args.W_mlp,input_ch=input_ch)
    grad_vars = list(model.parameters())
    optimizer = torch.optim.Adam(params=grad_vars, lr=args.lrate, betas=(0.9, 0.999))
    def query_fn(inputs): return run_MLP(inputs, model, embed_fn)
    return model, query_fn, optimizer

def img2mse(x, y): return torch.mean((x - y) ** 2)
def mse2psnr(x): return -10. * torch.log(x) / torch.log(torch.Tensor([10.]))

def img2scl(rgbs, target_emb, embed):
    rgbs_scl = rgbs.unsqueeze(0).permute(0, 3, 1, 2).clamp(0, 1)
    rgbs_resize_c = F.interpolate(rgbs_scl, size=(224, 224),
                                    mode="bicubic")
    rgbs_embed = embed(rgbs_resize_c)[0][0]
    cossim = -torch.cosine_similarity(target_emb, rgbs_embed, dim=-1)
    return cossim


def train():
    parser = parser = configargparse.ArgumentParser()
    parser.add_argument("--wandb_entity", type=str, default='felix-herrmann')
    parser.add_argument("--wandb_project", type=str, default='editnerf')
    parser.add_argument("--expname", type=str, default="image_reconstruction", help='experiment name')
    parser.add_argument("--lrate", type=float, default=1e-4, help='learning rate')
    parser.add_argument("--lrate_decay", type=int, default=250, help='exponential learning rate decay (in 1000 steps)')
    parser.add_argument("--loss", type=str, default='mse', choices=['mse', 'scl', 'mse_scl'])
    parser.add_argument("--img_fname", type=str, default='', help='target image file name')
    parser.add_argument("--i_img", type=int, default=100, help='log image')
    parser.add_argument("--i_scl", type=int, default=1, help='scl iterations')
    parser.add_argument("--n_iters", type=int, default=2000, help='number of iterations to train')
    parser.add_argument("--D_mlp", type=int, default=4, help='mlp layer depth')
    parser.add_argument("--W_mlp", type=int, default=256, help='mlp layer width')
    parser.add_argument("--i_embed", type=int, default=0, help='set 0 for default positional encoding, -1 for none')
    parser.add_argument("--multires", type=int, default=4, help='log2 of max freq for positional encoding (3D location)')
    parser.add_argument("--size", type=int, default=-1, nargs='+', help="Input image maxdim")
    parser.add_argument("--consistency_loss", type=str, default='none', choices=['none', 'consistent_with_target_rep'])
    parser.add_argument("--consistency_size", type=int, default=224)
    # parser.add_argument("--pixel_interp_mode", type=str, default='bicubic')
    # parser.add_argument("--feature_interp_mode", type=str, default='bilinear')
    parser.add_argument("--checkpoint_embedding", action='store_true')
    # Consistency model arguments
    parser.add_argument("--consistency_model_type", type=str,
                        default='clip_vit')  # choices=['clip_vit', 'clip_vit_b16', 'clip_rn50']
    parser.add_argument("--consistency_model_num_layers", type=int, default=-1)
    parser.add_argument("--clip_cache_root", type=str, default=os.path.expanduser("~/.cache/clip"))

    args = parser.parse_args()

    wandb.init(project=args.wandb_project, entity=args.wandb_entity)
    wandb.run.name = "{}_{}".format(args.expname, args.loss)
    wandb.run.save()
    wandb.config.update(args)

    # print(f'Using auxilliary consistency loss [{args.consistency_loss}], fine weight [{args.consistency_loss_lam}], coarse weight [{args.consistency_loss_lam0}]')
    embed = get_embed_fn(args.consistency_model_type, args.consistency_model_num_layers, checkpoint=args.checkpoint_embedding, clip_cache_root=args.clip_cache_root)


    # load image 
    img = imageio.imread(args.img_fname)
    img = torch.Tensor(img / 255.)
    coords = np.linspace(0, 1, img.shape[0], endpoint=False)
    x_test = torch.tensor(np.stack(np.meshgrid(coords, coords), -1))
    test_data = [x_test, img]
    # train_data = [x_test[::2, ::2], img[::2,::2]]
    train_data = [x_test, img]

    # create target embedding 
    with torch.no_grad():
        target_img = torch.tensor(img).unsqueeze(0).permute(0, 3, 1, 2).to(device)
        targets_resize_model = F.interpolate(target_img, (args.consistency_size, args.consistency_size),
                                                    mode="bicubic")
        target_embeddings = embed(targets_resize_model)
        target_emb = target_embeddings[0][0]
    

    # Create model
    model, query_fn, optimizer = create_MLP(args)
    

    for i in range(0, args.n_iters + 1):
        metrics = {}
        optimizer.zero_grad()

        # forward pass
        rgbs = query_fn(train_data[-1])

        mse = img2mse(rgbs, train_data[-1])
        psnr = mse2psnr(mse)

        loss = 0
        # loss
        if args.loss == "mse":
            loss = mse
        if args.loss == "scl":
            loss = img2scl(rgbs, target_emb, embed)
            scl = loss
        if args.loss == "mse_scl":
            loss = mse
            scl = img2scl(rgbs, target_emb, embed)
            if i % args.i_scl == 0:
                loss += 0.1 * img2scl(rgbs, target_emb, embed)

        # backward pass
        loss.backward()
        optimizer.step()

        if args.loss == "mse":
            with torch.no_grad():
                scl = img2scl(rgbs, target_emb, embed)
    
        metrics["train/mse"] = mse.item()
        metrics["train/psnr"] = psnr.item()
        
        if i % args.i_scl == 0:
            metrics["train/scl"] = scl.item()

        if i % args.i_img == 0:
            metrics["train/rgb"] = make_wandb_image(rgbs, preprocess='clip')
            print("mse: {:.4f}, psnr, {:.4f}, scl: {:.4f}".format(mse.item(), psnr.item(), scl.item()))

        if metrics:
            wandb.log(metrics, step=i)




if __name__ == '__main__':
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
    train()
