import os
import glob
import torch
import numpy as np
import shutil
import random
import json
import argparse

random.seed(0)

parser = argparse.ArgumentParser()
parser.add_argument(
    "--out_dir", required=True, help="Where to write the converted images"
)
parser.add_argument(
    "--src_dir",
    required=True,
    help="SRN chairs dir",
)

args = parser.parse_args()

# download NMR_Dataset.zip from pixel-nerf github. first link under 'getting the data'
# point data path to a object category subfolder of NMR dataset, NOT the main folder. Note: categories have cryptic names, i just renamed it here e.g. subfolder 03001627 from NMR is chairs
src_dir = args.src_dir
out_dir = args.out_dir

coord_trans_world = torch.tensor(
    [[1, 0, 0, 0], [0, 0, -1, 0], [0, 1, 0, 0], [0, 0, 0, 1]],
    dtype=torch.float32,
)
coord_trans_cam = torch.tensor(
    [[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]],
    dtype=torch.float32,
)
all_instances = []


os.mkdir(out_dir)
for instance in os.listdir(src_dir):
    if '.lst' in instance:
        continue

    all_instances.append(instance)
    root_dir = os.path.join(src_dir, instance)

    os.mkdir(os.path.join(out_dir, instance))
    os.mkdir(os.path.join(out_dir, instance, "train"))
    os.mkdir(os.path.join(out_dir, instance, "test"))

    transforms_train = {'frames': [], 'focal': -1.0}
    transforms_test = {'frames': [], 'focal': -1.0}
    transforms_dict = dict()


    rgb_paths = [
        x
        for x in glob.glob(os.path.join(root_dir, "image", "*"))
        if (x.endswith(".jpg") or x.endswith(".png"))
    ]


    cam_path = os.path.join(root_dir, 'cameras.npz')
    all_cam = np.load(cam_path)
    sel_indices = np.arange(len(rgb_paths))
    focal = 0.0

    test_idx = random.sample(list(sel_indices), 3)

    for idx, rgb_path in enumerate(rgb_paths):
        print(idx, rgb_path)

        # compute transform and focal
        i = sel_indices[idx]
        x_scale = 32 #img.shape[0]/2.0
        wmat_inv_key = "world_mat_inv_" + str(i)
        wmat_key = "world_mat_" + str(i)
        if wmat_inv_key in all_cam:
            extr_inv_mtx = all_cam[wmat_inv_key]
        else:
            extr_inv_mtx = all_cam[wmat_key]
            if extr_inv_mtx.shape[0] == 3:
                extr_inv_mtx = np.vstack((extr_inv_mtx, np.array([0, 0, 0, 1])))
            extr_inv_mtx = np.linalg.inv(extr_inv_mtx)

        intr_mtx = all_cam["camera_mat_" + str(i)]
        fx, fy = intr_mtx[0, 0], intr_mtx[1, 1]
        assert abs(fx - fy) < 1e-9
        fx = fx * x_scale
        focal = fx
        pose = extr_inv_mtx
        pose = (
            coord_trans_world
            @ torch.tensor(pose, dtype=torch.float32)
            @ coord_trans_cam
        )
        pose = pose.tolist()
        print(pose)
        print(focal)

        # copy image and add to json
        if i in test_idx:
            transforms_dict = transforms_test
            shutil.copy2(rgb_path, os.path.join(out_dir, instance, "test"))
            file_pth = os.path.join(out_dir, instance, "test", os.path.basename(rgb_path))
        else:
            transforms_dict = transforms_train
            shutil.copy2(rgb_path, os.path.join(out_dir, instance, "train"))
            file_pth = os.path.join(out_dir, instance, "train", os.path.basename(rgb_path))
            
        transforms_dict['frames'].append({'file_path': file_pth, 'transform_matrix': pose})
        transforms_dict['focal'] = focal

    with open(os.path.join(os.path.join(out_dir, instance), 'transforms_train.json'), 'w') as out_file:
        json.dump(transforms_train, out_file, indent=4)
    with open(os.path.join(os.path.join(out_dir, instance), 'transforms_test.json'), 'w') as out_file:
        json.dump(transforms_test, out_file, indent=4)
    with open(os.path.join(os.path.join(out_dir, instance), 'transforms_val.json'), 'w') as out_file:
        json.dump(transforms_test, out_file, indent=4)


with open(os.path.join(out_dir, 'instances.txt'), 'w') as f:
    for instance in all_instances:
        f.write(instance)
        f.write('\n')

