import os
import glob
import numpy as np
import json
from tqdm import tqdm
import shutil
import argparse



parser = argparse.ArgumentParser()
parser.add_argument(
    "--out_dir", required=True, help="Where to write the converted images"
)
parser.add_argument(
    "--src_dir",
    required=True,
    help="SRN chairs dir",
)

args = parser.parse_args()

src_dir = args.src_dir
out_dir = args.out_dir


all_instances = []

# readme
# place instances of shapenet in folder shapenet e.g. shapenet/{object_name}/
# structure should contain {object_name}/pose, {object_name}/rgb and {object_name}/intrinsics.txt

os.mkdir(out_dir)
for instance in tqdm(os.listdir(src_dir)):
    all_instances.append(instance)
    dir_path = os.path.join(src_dir, instance)

    intrin_path = os.path.join(dir_path, "intrinsics.txt")
    rgb_paths = glob.glob(os.path.join(dir_path, "rgb", "*"))
    pose_paths = glob.glob(os.path.join(dir_path, "pose", "*"))

    os.mkdir(os.path.join(out_dir, instance))
    os.mkdir(os.path.join(out_dir, instance, "train"))
    os.mkdir(os.path.join(out_dir, instance, "test"))
    os.mkdir(os.path.join(out_dir, instance, "val"))

    poses = []

    with open(intrin_path, "r") as intrinfile:
        lines = intrinfile.readlines()
        focal, _, _, _ = map(float, lines[0].split())
        height, width = map(int, lines[-1].split())

    transforms_train = {'frames': [], 'focal': focal} #TODO: Is this correct?
    transforms_test = {'frames': [], 'focal': focal}
    transforms_val = {'frames': [], 'focal': focal}
    transforms_dict = dict()

    i = 0
    for rgb_path, pose_path in zip(rgb_paths, pose_paths):
        pose = np.loadtxt(pose_path, dtype=np.float32).reshape(4, 4)
        poses.append(pose)

        file_pth = ""
        if i % 10 in (0, 1):
            transforms_dict = transforms_test
            shutil.copy2(rgb_path, os.path.join(out_dir, instance, "test"))
            file_pth = os.path.join(out_dir, instance, "test", os.path.basename(rgb_path))
        elif i % 10 in (2, 3):
            transforms_dict = transforms_val
            shutil.copy2(rgb_path, os.path.join(out_dir, instance, "val"))
            file_pth = os.path.join(out_dir, instance, "val", os.path.basename(rgb_path))
        else:
            transforms_dict = transforms_train
            shutil.copy2(rgb_path, os.path.join(out_dir, instance, "train"))
            file_pth = os.path.join(out_dir, instance, "train", os.path.basename(rgb_path))

        transforms_dict['frames'].append({'file_path': file_pth, 'transform_matrix': pose.tolist()})
        i += 1

    with open(os.path.join(os.path.join(out_dir, instance), 'transforms_train.json'), 'w') as out_file:
        json.dump(transforms_train, out_file, indent=4)
    with open(os.path.join(os.path.join(out_dir, instance), 'transforms_test.json'), 'w') as out_file:
        json.dump(transforms_test, out_file, indent=4)
    with open(os.path.join(os.path.join(out_dir, instance), 'transforms_val.json'), 'w') as out_file:
        json.dump(transforms_val, out_file, indent=4)


with open(os.path.join(out_dir, 'instances.txt'), 'w') as f:
    for instance in all_instances:
        f.write(instance)
        f.write('\n')


