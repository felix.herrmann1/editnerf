import os
import copy
import sys
import random

import numpy as np
import torch
from torch.utils.checkpoint import checkpoint as run_checkpoint
import torch.nn.functional as F
from scipy.spatial.transform import Rotation

import imageio
import wandb
import glob

from run_nerf_helpers import img2mse, mse2psnr, to8b, make_wandb_image, get_embed_fn, get_rays
import geometry
from inputs import config_parser
from dataset import load_data
from model import create_nerf
from rendering import render, render_path
from utils.pidfile import exit_if_job_done, mark_job_done
from load_blender import pose_spherical_uniform, pose_spherical

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def train():
    parser = config_parser()
    args = parser.parse_args()

    wandb.init(project=args.wandb_project, entity=args.wandb_entity)
    wandb.run.name = args.expname
    wandb.run.save()
    wandb.config.update(args)

    # Re-seed
    if args.seed == -1:
        args.seed = np.random.randint(2**32 - 1)

    print("SEED: {}".format(args.seed))
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)

    # Multi-GPU
    args.n_gpus = torch.cuda.device_count()
    print(f"Using {args.n_gpus} GPU(s).")

    # Create log dir and copy the config file
    basedir = args.basedir
    expname = args.savedir if args.savedir else args.expname
    print('Experiment dir:', expname)

    # Load data
    images, poses, style, i_test, i_train, i_val, bds_dict, dataset, hwfs, near_fars, style_inds, _ = load_data(args)
    images_test, poses_test, style_test, hwfs_test, nf_test = images[i_test], poses[i_test], style[i_test], hwfs[i_test], near_fars[i_test]
    images_train, poses_train, style_train, hwfs_train, nf_train = images[i_train], poses[i_train], style[i_train], hwfs[i_train], near_fars[i_train]
    images_val, poses_val, style_val, hwfs_val, nf_val = images[i_val], poses[i_val], style[i_val], hwfs[i_val], near_fars[i_val]

    n_testset_max = min(len(i_test), args.n_testset_max) if args.n_testset_max != -1 else len(i_test)
    n_trainset_max = min(len(i_train), args.n_trainset_max) if args.n_trainset_max != -1 else len(i_train)
    
    H, W, focal = hwfs[0]
    H, W = int(H), int(W)
    hwf = [H, W, focal]
    
    poses_render = torch.stack([pose_spherical(angle, -35, 1.581139) for angle in np.linspace(-180, 180, 90 + 1)[:-1]])
    poses_render[:,2,-1] += args.render_z_translation
    poses_render = poses_render.to(device)
    style_render = dataset.style[0].repeat(poses_render.shape[0], 1)
    hwfs_render = torch.tensor(hwf).repeat(poses_render.shape[0], 1)
    nfs_render = near_fars[0].repeat(poses_render.shape[0], 1)

    os.makedirs(os.path.join(basedir, expname), exist_ok=True)

    print("Number of instances: {}".format(torch.unique(style_inds).shape[0]))
    print("Image size: {}x{}".format(hwfs[0,0], hwfs[0,1]))
    print("Focal length: {}".format(hwfs[0,2]))
    print('NEAR FAR', near_fars[0,0], near_fars[0,1])
    print("Number of training views: {}".format(len(i_train)))
    print("Number of validation views: {}".format(len(i_val)))
    print("Number of testing views: {}".format(len(i_test)))
    print('TRAIN views are', i_train)
    print('TEST views are', i_test)
    print('VAL views are', i_val)


    np.save(os.path.join(basedir, expname, 'poses.npy'), poses_train.cpu())
    np.save(os.path.join(basedir, expname, 'hwfs.npy'), hwfs_train.cpu())
    f = os.path.join(basedir, expname, 'args.txt')
    with open(f, 'w') as file:
        for arg in sorted(vars(args)):
            attr = getattr(args, arg)
            file.write('{} = {}\n'.format(arg, attr))
    wandb.save(f)
    if args.config is not None:
        f = os.path.join(basedir, expname, 'config.txt')
        with open(f, 'w') as file:
            file.write(open(args.config, 'r').read())
    wandb.save(f)

    if args.consistency_loss != 'none':
        print(f'Using auxilliary consistency loss [{args.consistency_loss}], fine weight [{args.consistency_loss_lam}], coarse weight [{args.consistency_loss_lam0}]')
        embed = get_embed_fn(args.consistency_model_type, args.consistency_model_num_layers, checkpoint=args.checkpoint_embedding, clip_cache_root=args.clip_cache_root)

    
    # Create nerf model
    render_kwargs_train, render_kwargs_test, start, grad_vars, optimizer, scaler = create_nerf(args)

    wandb.watch(render_kwargs_train['network_fn'])

    old_coarse_network = copy.deepcopy(render_kwargs_train['network_fn']).state_dict()
    if args.N_importance > 0:
        print(render_kwargs_train['network_fine'])
        old_fine_network = copy.deepcopy(render_kwargs_train['network_fine']).state_dict()

    network_fn = render_kwargs_train['network_fn']
    network_fine = render_kwargs_train['network_fine']
    if args.checkpoint_rendering:
        # Pass a dummy input tensor that requires grad so checkpointing does something
        # https://discuss.pytorch.org/t/checkpoint-with-no-grad-requiring-inputs-problem/19117/10
        dummy = torch.ones(1, dtype=torch.float32, requires_grad=True, device=device)
        network_fn_wrapper = lambda x, a, b, c, y: network_fn(x, a, b, c)
        network_fine_wrapper = lambda x, a, b, c, y: network_fine(x, a, b, c)
        render_kwargs_train['network_fn'] = lambda x, a, b, c,: run_checkpoint(network_fn_wrapper, x, a, b, c, dummy)
        render_kwargs_train['network_fine'] = lambda x, a, b, c,: run_checkpoint(network_fine_wrapper, x, a, b, c, dummy)

    calc_ctr_loss = args.consistency_loss.startswith('consistent_with_target_rep')
    any_rendered_loss = calc_ctr_loss
    if any_rendered_loss:
        with torch.no_grad():
            targets = images.permute(0, 3, 1, 2).to(device)
            if args.render_nH < args.consistency_size or args.render_nW < args.consistency_size:
                targets = F.interpolate(targets, (args.render_nH, args.render_nW),
                                                     mode=args.pixel_interp_mode)

        # Embed training images for consistency loss
        if calc_ctr_loss:
            with torch.no_grad():
                targets_resize_model = F.interpolate(targets, (args.consistency_size, args.consistency_size),
                                                     mode=args.pixel_interp_mode)
                target_embeddings = embed(targets_resize_model)  # [N,L,D]

        # Embed training images for aligned consistency loss
        consistency_keep_keys = ['rgb_map', 'rgb0']

    global_step = start
    real_image_application = (args.real_image_dir is not None)
    optimize_mlp = not real_image_application
    render_kwargs_train.update(bds_dict)
    render_kwargs_test.update(bds_dict)
    loss = None

    if start == 0:
        # if we're starting from scratch, delete all the logs in that directory.
        if os.path.exists(os.path.join(basedir, expname, 'log.txt')):
            os.remove(os.path.join(basedir, expname, 'log.txt'))
    start = start + 1


    val_psnrs = np.zeros(args.n_iters+1)

    for i in range(start, args.n_iters + 1):
        metrics = {}

        render_loss_iter = i % args.render_loss_interval == 0

        if any_rendered_loss and render_loss_iter:
            with torch.no_grad():
                # Render from a random viewpoint
                if args.render_poses == 'loaded':
                    poses_i = np.random.choice(i_train)
                    pose = poses[poses_i, :3, :4]
                elif args.render_poses == 'interpolate_train_all':
                    assert len(i_train) >= 3
                    poses_i = np.random.choice(i_train, size=3, replace=False)
                    pose1, pose2, pose3 = poses[poses_i, :3, :4].cpu()
                    s12, s3 = np.random.uniform(*args.render_poses_interpolate_range, size=2)
                    pose = geometry.interp3(pose1, pose2, pose3, s12, s3)
                elif args.render_poses == 'uniform':
                    # assert args.dataset_type == 'blender'
                    pose = pose_spherical_uniform(args.render_theta_range, args.render_phi_range,
                                                  args.render_radius_range)
                    pose = pose[:3, :4]
                    pose[2,-1] += args.render_z_translation

                print('Sampled pose:', Rotation.from_matrix(pose[:, :3].cpu()).as_rotvec(), 'origin:', pose[:, 3])

                if args.render_poses_translation_jitter_sigma > 0:
                    pose[:, -1] = pose[:, -1] + torch.randn(3, device=pose.device) * args.render_poses_translation_jitter_sigma

                H, W, focal = hwfs[0]
                # TODO: something strange with pts_W in get_rays when 224 nH
                rays = get_rays(H, W, focal.to(device), c2w=pose.to(device), nH=args.render_nH, nW=args.render_nW)
                
            with torch.cuda.amp.autocast(enabled=args.render_autocast):
                i_target_emb = np.random.choice(args.N_instances)
                style = torch.stack([dataset.style[i_target_emb] for _ in range(args.render_nH * args.render_nW)])
                # TODO: check usage of viewdirs_reg=viewdirs_reg in render()
                extras = render(H, W, focal.to(device), style=style, chunk=args.chunk,
                                keep_keys=consistency_keep_keys,
                                rays=(rays[0].to(device), rays[1].to(device)),
                                **render_kwargs_train)[-1]
                # rgb0 is the rendering from the coarse network, while rgb_map uses the fine network
                if args.N_importance > 0:
                    rgbs_consistency = torch.stack([extras['rgb_map'], extras['rgb0']], dim=0)
                else:
                    rgbs_consistency = extras['rgb_map'].unsqueeze(0)
                rgbs_consistency = rgbs_consistency.permute(0, 3, 1, 2).clamp(0, 1)

            if i == 0:
                print('rendering losses rendered rgb image shape:', rgbs_consistency.shape)

            # Log rendered images
            metrics['train_ctr/rgb'] = make_wandb_image(extras['rgb_map'], 'clip')
            if args.N_importance > 0:
                metrics['train_ctr/rgb0'] = make_wandb_image(extras['rgb0'], 'clip')

        #####  Core optimization loop  #####
        optimizer.zero_grad()
        loss = 0

        if calc_ctr_loss and render_loss_iter:
            assert args.consistency_loss == 'consistent_with_target_rep'

            # Resize and embed rendered images
            rgbs_resize_c = F.interpolate(rgbs_consistency, size=(args.consistency_size, args.consistency_size),
                                          mode=args.pixel_interp_mode)
            rendered_embeddings = embed(rgbs_resize_c)
            rendered_embedding = rendered_embeddings[0]
            if args.N_importance > 0:
                rendered_embedding0 = rendered_embeddings[1]  # for coarse net

            # Randomly sample a target
            if args.consistency_model_type.startswith('clip_vit'):
                # Modified CLIP ViT to return sequence features. Extract the [CLS] token features
                assert rendered_embedding.ndim == 2  # [L,D]
                assert target_embeddings.ndim == 3  # [N,L,D]
                rendered_emb = rendered_embedding[0]
                if args.N_importance > 0:
                    rendered_emb0 = rendered_embedding0[0]
                if args.mean_embedding:
                    target_emb = target_embeddings[dataset.instance_to_train_image_id[i_target_emb], 0] 
                else:
                    target_emb =  torch.unsqueeze(target_embeddings[np.random.choice(dataset.instance_to_train_image_id[i_target_emb]), 0],0)
            else:
                assert rendered_embedding.ndim == 1  # [D]
                assert target_embeddings.ndim == 2  # [N,D]
                rendered_emb, target_emb = rendered_embedding, target_embeddings
                if args.N_importance > 0:
                    rendered_emb0 = rendered_embedding0

            # Sample a single random target for consistency loss
            consistency_loss = torch.mean(torch.stack([-torch.cosine_similarity(target_emb[i], rendered_emb, dim=-1) for i in range(target_emb.shape[0])]))
            if args.N_importance > 0:
                consistency_loss0 = torch.mean(torch.stack([-torch.cosine_similarity(target_emb[i], rendered_emb0, dim=-1) for i in range(target_emb.shape[0])]))

            if i >= args.sc_loss_start and i <= args.sc_loss_end:
                lambd = args.consistency_loss_lam
                lambd0 = args.consistency_loss_lam0
            else:
                lambd = 0.0
                lambd0 = 0.0
 
            loss = loss + consistency_loss * lambd
            if args.N_importance > 0:
                loss = loss + consistency_loss0 * lambd0

        # Sample random ray batch
        batch_rays, target_s, style, H, W, focal, near, far, viewdirs_reg = dataset.get_data_batch()
        render_kwargs_train.update({'near': near, 'far': far})
        rgb, _, _, extras = render(H, W, focal, style=style, chunk=args.chunk, rays=batch_rays, viewdirs_reg=viewdirs_reg, **render_kwargs_train)

        img_loss = img2mse(rgb, target_s)
        if not args.no_mse:
            loss = loss + img_loss
        psnr = mse2psnr(img_loss)

        if args.var_param > 0:
            var = extras['var']
            var0 = extras['var0']
            var_loss = var.mean(dim=0)
            var_loss_coarse = var0.mean(dim=0)

            if not args.no_mse:
                loss += args.var_param * var_loss
                loss += args.var_param * var_loss_coarse
            var_loss = var_loss.item()
            var_loss_coarse = var_loss_coarse.item()
        else:
            var_loss = 0
            var_loss_coarse = 0

        if 'rgb0' in extras:
            img_loss0 = img2mse(extras['rgb0'], target_s)
            if not args.no_mse:
                loss = loss + img_loss0
            psnr0 = mse2psnr(img_loss0).item()
        else:
            psnr0 = -1

        if args.weight_change_param >= 0:
            weight_change_loss_coarse = 0.
            for k, v in network_fn.named_parameters():
                if 'weight' in k:
                    diff = (old_coarse_network[k] - v).pow(2).mean()
                    weight_change_loss_coarse += diff
            weight_change_loss_fine = 0.
            if args.N_importance > 0:
                for k, v in network_fine.named_parameters():
                    if 'weight' in k:
                        diff = (old_fine_network[k] - v).pow(2).mean()
                        weight_change_loss_fine += diff
            weight_change_loss = weight_change_loss_coarse + weight_change_loss_fine
            
            if not args.no_mse:
                loss = loss + args.weight_change_param * weight_change_loss
        else:
            weight_change_loss = torch.tensor(0.)

        scaler.scale(loss).backward()
        if optimize_mlp:
            scaler.step(optimizer)
            scaler.update()

        if args.use_styles:
            dataset.optimize_styles(train_fn=render_kwargs_train, optimizer=optimizer, loss=loss)

        # NOTE: IMPORTANT!
        decay_rate = 0.1
        decay_steps = args.lrate_decay * 1000
        new_lrate = args.lrate * (decay_rate ** (global_step / decay_steps))

        for param_group in optimizer.param_groups:
            param_group['lr'] = new_lrate
        ################################
        #####           end            #####

        if i >= 2000 and args.stop_if_background:
            bg_img = torch.full_like(rgb, 1.) if args.white_bkgd else torch.full_like(rgb, 0.)
            if torch.equal(rgb, bg_img) or torch.equal(extras['rgb0'], bg_img):
                print('Stuck in background color, stop training.')
                sys.exit(1)
                break

        if i % args.i_weights == 0:
            path = os.path.join(basedir, expname, '{:06d}.tar'.format(i))
            state_dict = {
                'global_step': global_step + 1,
                'network_fn_state_dict': network_fn.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'styles': dataset.style,
                'style_optimizer': dataset.style_optimizer.state_dict()
            }
            if args.N_importance > 0:
                state_dict['network_fine_state_dict'] = network_fine.state_dict()
            torch.save(state_dict, path)
            wandb.save(path)
            print('Saved checkpoints at', path)

        if i % args.i_testset == 0 and i > 0:
            if real_image_application:
                style_test = dataset.get_features().repeat((poses_test.shape[0], 1))
            testsavedir = os.path.join(basedir, expname, 'testset_{:06d}'.format(i))
            os.makedirs(testsavedir, exist_ok=True)
            with torch.no_grad():
                _, _ , total_psnr = render_path(poses_test.to(device), style_test, hwfs_test, args.chunk, render_kwargs_test, gt_imgs=images_test, nfs=nf_test,
                            savedir=testsavedir, maximum=n_testset_max)
            metrics['testset/psnr'] = total_psnr
            for img_dir in glob.glob(os.path.join(testsavedir, "*.png")):
                wandb.save(img_dir)
            print(f'Saved test set to {testsavedir}'.format())


        if i % args.i_trainset == 0 and i > 0:
            if real_image_application:
                style_train = dataset.get_features().repeat((poses_train.shape[0], 1))
            trainsavedir = os.path.join(basedir, expname, 'trainset_{:06d}'.format(i))
            os.makedirs(trainsavedir, exist_ok=True)
            with torch.no_grad():
                render_path(poses_train.to(device), style_train, hwfs_train, args.chunk, render_kwargs_test,
                            nfs=nf_train, savedir=trainsavedir, maximum=n_trainset_max)
            for img_dir in glob.glob(os.path.join(trainsavedir, "*.png")):
                wandb.save(img_dir)
            print('Saved train set')

        if render_loss_iter and calc_ctr_loss:
            metrics["train_ctr/consistency_loss"] = consistency_loss.item()
            if args.N_importance > 0:
                metrics["train_ctr/consistency_loss0"] = consistency_loss0.item()

        if i % args.i_print == 0 or i == 1:
            log_str = f"[TRAIN] Iter: {i} Loss: {loss.item()} PSNR: {psnr.item()} PSNR0: {psnr0} Var loss: {var_loss} Var loss coarse: {var_loss_coarse} Weight change loss: {weight_change_loss}"
            with open(os.path.join(basedir, expname, 'log.txt'), 'a+') as f:
                f.write(log_str + '\n')
            print(log_str)
            
            metrics.update({
                "train/loss": loss,
                "train/psnr": psnr,
                "train/mse": img_loss,
                "train/lrate": new_lrate
            })
            if args.N_importance > 0:
                metrics["train/psnr0"] = psnr0
                metrics["train/mse0"] = img_loss0
        
        if i % args.i_eval == 0 or i == 1:
            batch_rays, target_s, style, H, W, focal, near, far, viewdirs_reg = dataset.get_data_batch(split="eval")
            render_kwargs_train.update({'near': near, 'far': far})
            with torch.no_grad():
                rgb, _, _, extras = render(H, W, focal, style=style, chunk=args.chunk, rays=batch_rays, viewdirs_reg=viewdirs_reg, **render_kwargs_train)

            img_loss = img2mse(rgb, target_s)
            loss = img_loss
            psnr = mse2psnr(img_loss)

            val_psnrs[i] = psnr

            if 'rgb0' in extras:
                img_loss0 = img2mse(extras['rgb0'], target_s)
                loss = loss + img_loss0
                psnr0 = mse2psnr(img_loss0).item()
            else:
                psnr0 = -1

            log_str = f"[VAL] Iter: {i} Loss: {loss.item()} PSNR: {psnr.item()} PSNR0: {psnr0}"
            with open(os.path.join(basedir, expname, 'log.txt'), 'a+') as f:
                f.write(log_str + '\n')
            print(log_str)

            metrics.update({
                "val/loss": loss,
                "val/psnr": psnr,
                "val/mse": img_loss
            })
        
        if i % args.i_render_train == 0:
            i_img = 0
            pose = poses[i_img].squeeze().cuda()
            style = dataset.style[dataset.style_inds[i_img]].squeeze().cuda()
            with torch.no_grad():
                rgb, _, _, extras = render(H, W, focal, style=style, chunk=args.chunk,
                                                    c2w=pose[:3, :4], **render_kwargs_test)

            metrics["train/rgb"] = make_wandb_image(rgb, preprocess='clip')

            if args.N_importance > 0:
                metrics["train/rgb0"] = make_wandb_image(extras['rgb0'], preprocess='clip')

        if i % args.i_render_eval == 0:
            i_img = 0
            pose = poses_val[i_img].squeeze().cuda()
            style = style_val[i_img].squeeze().cuda()
            target = images_val[i_img].squeeze().cuda()
            with torch.no_grad():
                rgb, _, _, extras = render(H, W, focal, style=style, chunk=args.chunk,
                                                    c2w=pose[:3, :4], **render_kwargs_test)

            metrics.update({
                "val/rgb": make_wandb_image(rgb, preprocess='clip')
            })
            if args.N_importance > 0:
                metrics["val/rgb0"] = make_wandb_image(extras['rgb0'], preprocess='clip')

            valsavedir = os.path.join(basedir, expname, 'valset'.format(i))
            os.makedirs(valsavedir, exist_ok=True)
            imageio.imwrite(os.path.join(valsavedir, '{:04d}_rgb.png'.format(i)), to8b(rgb.cpu().numpy()))
            print('Saved val set')
        
        if i%args.i_video==0 and i > 0:
            # Turn on testing mode
            with torch.no_grad():
                rgbs, disps, psnr = render_path(poses_render, style_render, hwfs_render, args.chunk, render_kwargs_test)
                print('Done, saving', rgbs.shape, disps.shape)
                moviebase = os.path.join(basedir, expname, '{}_circle_{:06d}_'.format(expname, i))
                imageio.mimwrite(moviebase + 'rgb.mp4', to8b(rgbs), fps=20, quality=8)
                imageio.mimwrite(moviebase + 'disp.mp4', to8b(disps / np.max(disps)), fps=20, quality=8)
                metrics["render_path/rgb_video"] = wandb.Video(moviebase + 'rgb.mp4')
                metrics["render_path/disp_video"] = wandb.Video(moviebase + 'disp.mp4')
                imageio.mimsave(moviebase + 'rgb.gif', to8b(rgbs), fps=20)
                imageio.mimsave(moviebase + 'disp.gif', to8b(disps), fps=20)

        if metrics:
            wandb.log(metrics, step=i)

        if i==args.val_stop_iter:
            if np.max(val_psnrs) < args.val_stop:
                print("Exiting, no increase in val psnr")
                sys.exit(1)


        global_step += 1

        if real_image_application and global_step - start == args.n_iters_real:
            return

        if real_image_application and global_step - start == args.n_iters_code_only:
            optimize_mlp = True
            dataset.optimizer_name = 'adam'
            dataset.style_optimizer = torch.optim.Adam(dataset.params, lr=dataset.lr)
            print('Starting to jointly optimize weights with code')


if __name__ == '__main__':
    parser = config_parser()
    args = parser.parse_args()
    if args.instance != -1:
        # Allows for scripting over single instance experiments.
        exit_if_job_done(os.path.join(args.basedir, args.expname))
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        train()
        mark_job_done(os.path.join(args.basedir, args.expname))
    else:
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
        train()
